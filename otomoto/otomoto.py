import requests
from bs4 import BeautifulSoup
import re
import json

def jedna_spacja(t):
    return " ".join(t.split())

numery = []
miasta = []

s = requests.Session()

for i in range(1,2):
    
    link = 'https://www.otomoto.pl/maszyny-rolnicze/?page='+str(i)

    r = s.get(link)

    soup = BeautifulSoup(r.text, "html.parser")

    h2s = soup.find_all("h2")
    h4s = soup.find_all("h4")
    for h2, h4 in zip(h2s,h4s):

        ID = h2.a["href"]

        ID = re.search('-ID(.+?).html', ID).group(1)

        ajax = 'https://www.otomoto.pl/ajax/misc/contact/multi_phone/'+ID+'/0/'
        ajax = s.get(ajax)
        if ajax.status_code == 200:
            ajax = json.loads(ajax.text)["value"]
            ajax = bez_spacji(ajax)
            if ajax.startswith('+48'):
                ajax = ajax[3:]
            elif ajax.startswith('48'):
                ajax = ajax[2:]
            numery.append(ajax)
            miasto = jedna_spacja(h4.text)
            miasta.append(miasto)

for rekord in set(zip(numery, miasta)):
    print (rekord)
