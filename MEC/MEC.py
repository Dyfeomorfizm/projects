import os
import json
import re
import datetime

import pandas as pd


LOGI = "./dane/logs/"
TAG_DICT = "./dane/tag_to_name"

def nawiasy(tekst):
    x = 0
    for znak in tekst:
        if znak == "{":
            x += 1
        elif znak == "}":
            x -= 1
    return x

i = 0
lista = []
times = []

for filename in os.listdir(LOGI):
    with open(os.path.join(LOGI, filename), 'r') as f:
        for line in f:
            try:
                lista.append(json.loads(line)["nameValuePairs"])
                times.append(json.loads(line)["timestamp"])
                i += 1
            except ValueError:
                if nawiasy(line):
                    line = line[:-1] + "}"
                    lista.append(json.loads(line)["nameValuePairs"])
                    times.append(json.loads(line)["timestamp"])
            
print("liczba poprawnych wpisów: ", i)

df = pd.DataFrame(lista)

procenty = ((df['ismobile'].value_counts()/len(lista)).values * 100).round()

print("Mobile {}%\nNot mobile {}%".format(int(procenty[1]), int(procenty[0])))

krolowie = {}
with open(TAG_DICT) as f:
    for line in f:
        found = re.match('(.+):(.+)',line)
        krolowie[found.group(1)] = found.group(2)

zad3 = df['tagid'].value_counts()

tagi = [krolowie[k] for k in zad3.keys()]

for k,v in zip(tagi,zad3.values):
    print(k,v)

times = [pd.Timestamp(t) for t in times]
times15=[]
for t in times:
    times15.append(datetime.datetime(t.year, t.month, t.day, t.hour,
                                    15*(t.minute//15)))
dft = pd.DataFrame(times15)
print(dft[0].value_counts().keys()[0])
