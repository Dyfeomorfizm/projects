from toolsy.analyzers import pre, tok
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.feature_extraction.text import _document_frequency
from sklearn.preprocessing import normalize
import numpy as np
import scipy.sparse as sp

class Analizator():
    def __init__(self,RTD, vectorizer=CountVectorizer(preprocessor=pre,
                                                    tokenizer=tok),
                 transformer=TfidfTransformer(norm=None, smooth_idf=False)):
        self.vectorizer = vectorizer
        self.analyzer = vectorizer.build_analyzer()
        self.transformer = transformer
        self.RTD = RTD
        self.voc = None
        

    def get_voc(self, t, y):
        self.voc = set(self.analyzer(t)).intersection(set(self.analyzer(y)))

    def I(self, t, y):
        t, y = normalize(t), normalize(y)
        return t.dot(y.T)[0,0]

    def C(self, t, y):
        licznik = 0
        for k in range(t.shape[1]):
            licznik += min(t[0,k], y[0,k])
        mianownik = min(t.sum(),y.sum())
        return licznik / mianownik

    def ind(self, t, y, e=None):
        wynik = []
        for i in range(t.shape[1]):
            if not t[0,i] != y[0,i]:
                wynik.append(0)
            elif t[0,i] < y[0,i] and e != 1:
                wynik.append(1)
            elif e != 2:
                wynik.append(2)
        return wynik

    def R(self, t, y):
        licznik = 2 * max(len(self.ind(t, y, e=1)), len(self.ind(t, y, e=2)))
        mianownik = len(self.voc)
        return (licznik/mianownik) - 1

    def g(self, u):
        print(u)
        if u == 1.0:
            return 1
        else:
            return 1 - np.sqrt(1 -(1 -(2 * np.arccos(u)/np.pi))**2)

    def fi(self, x, y, z):
        return self.g(max(x, y*z))

    def set_tf(self):
        self.RTD_tf = self.vectorizer.fit_transform(self.RTD)

    def set_df(self):
        self.RTD_df = _document_frequency(self.RTD_tf)

    def test(self, t):
        
        self.set_tf()
        self.set_df()
        self.global_vocabulary = self.vectorizer.vocabulary_

        wyniki = []
        
        for y in self.RTD:

            self.get_voc(t, y)
            
            if len(self.voc):
                
                self.vectorizer.vocabulary = self.voc

                self.t_tf = self.vectorizer.fit_transform([t])
                self.y_tf = self.vectorizer.fit_transform([y])

                self.sorted_voc = list(self.voc)
                self.sorted_voc.sort()
                
                voc_indices = [self.global_vocabulary[s] for s in self.sorted_voc]

                df = np.array([self.RTD_df[i] for i in voc_indices])

                idf = np.array(
                    [np.log(self.RTD_tf.shape[0]/dfi) for dfi in df]
                    )
                
                self.t_tfidf = sp.csr_matrix(self.t_tf.A*idf)
                self.y_tfidf = sp.csr_matrix(self.y_tf.A*idf)

                i = self.I(self.t_tfidf, self.y_tfidf)
                c = self.C(self.t_tfidf, self.y_tfidf)
                r = self.R(self.t_tfidf, self.y_tfidf)

                wyniki.append(self.fi(i,c,r))

            else:
                wyniki.append(0)

        print(wyniki)
        
