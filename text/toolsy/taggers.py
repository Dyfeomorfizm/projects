from nltk.tag import UnigramTagger, BigramTagger, TrigramTagger, DefaultTagger
from nltk.corpus import treebank
import pickle
def backoff_tagger():
    try:
        
        f = open('tagger.pickle', 'rb')
        tagger = pickle.load(f)
        return tagger
    except:
        
        def do_backoff(train_sents, tagger_classes, backoff=None):
            for cls in tagger_classes:
                backoff = cls(train_sents, backoff=backoff)

            return backoff

        backoff = DefaultTagger('NN')
        tagger = do_backoff(treebank.tagged_sents(),
                            [UnigramTagger, BigramTagger, TrigramTagger],
                            backoff=backoff)
        return tagger
