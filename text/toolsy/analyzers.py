import re

from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords, wordnet

from toolsy.replacers import RegexpReplacer, AntonymSynonymReplacer
from toolsy.taggers import backoff_tagger
from toolsy.pos import get_wordnet_pos as gwp

def lower(x):
    return x.lower()

def replace(x):
    replacer = RegexpReplacer()
    return replacer.replace(x)

def tokenize(examples):
    tokenizer = RegexpTokenizer("[A-Za-z]+")
    return tokenizer.tokenize(examples)

def tag(examples):
    tagger = backoff_tagger()
    return tagger.tag(examples)

def lemmatize(examples):
    lemmatizer = WordNetLemmatizer()
    return [(lemmatizer.lemmatize(e[0],pos=gwp(e[1])),e[1]) for e in examples]

def synonymize(examples):
    synonymizer = AntonymSynonymReplacer()
    return synonymizer.replace(examples)

def untag(examples):
    return [e[0] for e in examples]

def stoper(examples, stops):
    return [e for e in examples if e not in stops]

stops = set(stopwords.words('english'))

def pre(x):
    return replace(lower(x))

def tok(x):
    return stoper(untag(synonymize(lemmatize(tag(tokenize(x))))), stops)
