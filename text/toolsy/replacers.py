import re

import enchant
from nltk.metrics import edit_distance
from nltk.corpus import wordnet, stopwords
from toolsy.pos import get_wordnet_pos

replacement_patterns = [
    (r'won\'t', 'will not'),
    (r'can\'t', 'can not'),
    (r'i\'m', 'i am'),
    (r'ain\'t', 'is not'),
    (r'(\w+)\'ll', '\g<1> will'),
    (r'(\w+)n\'t', '\g<1> not'),
    (r'(\w+)\'ve', '\g<1> have'),
    (r'(\w+)\'s', '\g<1> is'),
    (r'(\w+)\'re', '\g<1> are'),
    (r'(\w+)\'d', '\g<1> would')
    ]

english_stops = set(stopwords.words('english'))

class RegexpReplacer(object):
    def __init__(self, patterns=replacement_patterns):
        self.patterns = [(re.compile(regex), repl) for (regex, repl) in
                         patterns]

    def replace(self, text):
        s = text
        for (pattern, repl) in self.patterns:
            s = re.sub(pattern, repl, s)
        return s

class AntonymSynonymReplacer(object):
    def antonyms(self, word, pos=None):
        antonym = set()
        for syn in wordnet.synsets(word, pos=pos):
            for lemma in syn.lemmas():
                for ant in lemma.antonyms():
                    antonym.add(ant.name())
        if len(antonym) == 1:
            return antonym.pop().lower()
        else:
            return None

    def synonyms(self, word, pos=None):
        synonym = word
        max_count = 0
        for syn in wordnet.synsets(word, pos=pos):
            for lemma in syn.lemmas():
                if lemma.count() > max_count:
                    synonym = lemma.name()
                    max_count = lemma.count()
        return synonym.lower()

    def replace(self, sent):
        i, length = 0, len(sent)
        words = []
        while i < length:
            if sent[i][0] == 'not' and i+1 < length:
                if sent[i+1][0] not in english_stops:
                    pos = get_wordnet_pos(sent[i+1][1])
                    ant = self.antonyms(sent[i+1][0], pos=pos)
                    if ant:
                        words.append((self.synonyms(ant, pos=pos), sent[i+1][1]))
                        i += 2
                        continue
            else:
                if sent[i][0] not in english_stops:
                    pos = get_wordnet_pos(sent[i][1])
                    syn = self.synonyms(sent[i][0], pos=pos)
                    words.append((syn,sent[i][1]))
                    i += 1
                    continue
            words.append((sent[i][0],sent[i][1]))
            i += 1
        return words

class SpellingReplacer(object):
    def __init__(self, dict_name='en', max_dist=2):
        self.spell_dict = enchant.Dict(dict_name)
        self.max_dist = max_dist

    def replace(self, word):
        if self.spell_dict.check(word):
            return word
        suggestions = self.spell_dict.suggest(word)

        if (suggestions and edit_distance(word, suggestions[0]) <=
            self.max_dist):
            return suggestions[0]
        else:
            return word
