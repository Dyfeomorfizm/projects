def doc_summary(module_name, filename):
    def get_first_paragraph(text):
        paragraphs = text.split("\n\n")
        return paragraphs[0]

    def importing(module_name):
        module = __import__(module_name)
        return module

    def create_doc(module):
        doc = module.__name__ + "\n" + get_first_paragraph(module.__doc__)
        for m in dir(module):
            struct = getattr(module, m)
            try:
                name = struct.__name__
                doc += "\n----------------\n"
                doc += name + "\n" + get_first_paragraph(str(struct.__doc__))
            except:
                pass
        return doc

    doc = create_doc(importing(module_name))
    
    with open(filename, 'w') as f:
        f.write(doc)
        f.close()
