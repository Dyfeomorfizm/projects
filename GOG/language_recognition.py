import random

import requests
import numpy as np
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB

"""

Run script to download data and train classifier. It will take 30 minutes.

Then predict(filename) to predict.

"""

def get_data():
    session = requests.session()
    r = session.get("https://github.com/login")
    soup = BeautifulSoup(r.text,"html.parser")
    inputs = soup.find_all("input")
    a_token = inputs[1]["value"]
    payload = {'login': 'f4keUser',
               'password': 'p4ssword',
               'authenticity_token': a_token}
    r = session.post("https://github.com/session", params=payload)
    texts = [[],[],[]]
    params = [["python","py"],["java","java"],["cpp","cpp"]]
    for j in range(3):
        for i in range(1,101):
            url = "https://github.com/search?p={}&q={}+in%3Apath+extension%3A{}&type=Code".format(str(i),params[j][0],params[j][1])
            r = session.get(url)
            soup = BeautifulSoup(r.text,"html.parser")
            links = soup.select('a[href$=".{}"]'.format(params[j][1]))
            links = [link["href"] for link in links[::2]]
            for link in links:
                link = link.replace("/blob/","/raw/")
                print (link)
                r = session.get("https://github.com{}".format(link))
                texts[j].append(r.text)
    return texts

texts = get_data()

def train_and_test(texts, test_size=0.2):
    y = np.array((len(texts[0])*["python"]) +
                 (len(texts[1])*["java"]) +
                 (len(texts[2])*["cpp"]))
    flatten_texts = [item for sublist in texts for item in sublist]
    zipped = list(zip(flatten_texts, y))
    random.shuffle(zipped)
    X, y = zip(*zipped)
    size = int(len(X) * test_size)
    X_train = X[:size]
    X_test = X[size:]
    y_train = y[:size]
    y_test = y[size:]
    return X_train, X_test, y_train, y_test

X_train, X_test, y_train, y_test = train_and_test(texts)
    
def fit(X_train, y_train):
    vectorizer = CountVectorizer()
    X_train = vectorizer.fit_transform(X_train)
    tf_transformer = TfidfTransformer(use_idf=False).fit(X_train)
    X_train_tf = tf_transformer.transform(X_train)
    clf = MultinomialNB().fit(X_train_tf,y_train)
    print("Classifier trained")
    return clf,vectorizer,tf_transformer

clf,vectorizer,tf_transformer = fit(X_train, y_train)

def accuracy(arr_1, arr_2):
    return sum([i == j for i,j in zip(arr_1, arr_2)])/float(len(arr_1))

def test(X_test, y_test):
    X_test = vectorizer.transform(X_test)
    X_test_tf = tf_transformer.transform(X_test)
    predictions = clf.predict(X_test_tf)
    print("Accuracy score = {}".format(accuracy(predictions, y_test)))

test(X_test, y_test)

def predict(filename):
    with open(filename) as f:
        docs_new = [f.read()]
    X_new = vectorizer.transform(docs_new)
    X_new_tf = tf_transformer.transform(X_new)
    predicted = clf.predict(X_new_tf)
    print(predicted)
