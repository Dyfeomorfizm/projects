




##Zadanie 1


def equilibrum(lista):
    suma = sum(lista)
    suma_lewa = 0
    for i in range(len(lista)):
        suma_lewa += lista[i]
        suma_prawa = suma - suma_lewa
        if suma_lewa > suma_prawa:
            return i





##Zadanie 2


def nawiasy(tekst):
    x = 0
    for znak in tekst:
        if znak == "(":
            x += 1
        elif znak == ")":
            x += -1
        if x < 0:
            return False
    if x == 0:
        return True
    else:
        return False

##Jeśli jest ok nic się nie dzieje

import unittest

class Testy(unittest.TestCase):
    def test_a(self):
        self.assertFalse(nawiasy('(a(c)'))
        
    def test_b(self):
        self.assertFalse(nawiasy(')b'))
        
    def test_c(self):
        self.assertTrue(nawiasy('(b(s))'))




##Zadanie 3



import numpy as np
from sklearn.cross_validation import KFold
import pandas as pd

def predict(x_vals, weights):
    return sigmoid(x_vals.dot(weights))

def cost_function(x_vals, y_vals, weights):
    n = y_vals.size
    prediction = predict(x_vals, weights)
    error = prediction - y_vals
    try:
        cost = (1.0/(2.0*n)) * error.T.dot(error)
    except AttributeError:
        cost = (1.0/(2.0*n)) * error ** 2.0
    return cost

def update(x_vals, y_vals, learn_rate, weights):
    preds = predict(x_vals, weights)
    g = (preds - y_vals) * x_vals
    weights -= learn_rate * g * (1.0 / y_vals.size)
    return weights

def sigmoid(x):
    result = 1.0 / (1.0 + np.e**-x)
    return result

def cross_validate(X, y, learn_rate=0.001, max_epochs=30, k_fold=10):
    num_observations, num_params = X.shape
    kf = KFold(num_observations, n_folds=k_fold, random_state=1, shuffle=True)
    IndexList = list(kf)
    scores_and_labels = []
    train_error = []
    test_error = []

    for fold in range(k_fold):
        weights = np.zeros(num_params)
        train_error.append([])
        test_error.append([])

        train_index, test_index = IndexList[fold]
        train_x, test_x = X.iloc[train_index,:], X.iloc[test_index,:]
        train_y, test_y = y.iloc[train_index], y.iloc[test_index]

        train_error[fold].append(cost_function(train_x, train_y, weights))
        test_error[fold].append(cost_function(test_x, test_y, weights))
        counter = 0
        while counter < max_epochs:
            counter += 1

            
            for i in range(len(train_y)):
                weights = update(train_x.iloc[i], train_y.iloc[i],
                                 learn_rate, weights)

            train_error[fold].append(cost_function(train_x, train_y, weights))
            test_error[fold].append(cost_function(test_x, test_y, weights))

        scores_and_labels += [list(i) for i in zip(predict(test_x, weights),
                                                   test_y)]

    train_error = np.mean(np.array(train_error), axis=0)
    test_error = np.mean(np.array(test_error), axis=0)
    scores_and_labels = np.array(scores_and_labels)

    return train_error, test_error, scores_and_labels, weights

data = pd.read_csv("spambase.data", sep=",", header=None)


X = data.iloc[:,:-1]
y = data.iloc[:,-1]

for i in X:
    X[i] = (X[i] - X[i].mean())/X[i].std()

X = pd.DataFrame(np.hstack((np.ones((X.shape[0],1)), X)))

(train_error, test_error, scores_and_labels, weights) = cross_validate(X, y)






##Zadanie 4


def solution(T):
    def partial(T):
        if T is None:
            return [-1,-1,-1]
        else :
            left = partial(T.l)
            right = partial(T.r)
            left[0] += 1
            right[1] += 1
            maks = max(max(left[0],right[1]),max(left[2],right[2]))
            return [left[0], right[1], maks]
    return partial(T)[2]


