#!/usr/bin/env python
# -*- coding: utf-8 -*-

import asyncio
import json
import sys

import pika
from aiohttp import web

@asyncio.coroutine
def handle(request):
    name = request.match_info.get('name', 'Anonymous')
    data = yield from request.post()
    message = {name:data[name]}
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=sys.argv[1]))
    channel = connection.channel()

    channel.queue_declare(queue='hello')
    
    channel.basic_publish(exchange='',
                  routing_key='hello',
                  body=json.dumps(message))
    print(" [x] Sent ", message)
    connection.close()
    
    return web.Response(text = "Sent " + str(message))

app = web.Application()
app.router.add_post('/', handle)
app.router.add_post('/{name}', handle)

web.run_app(app, host='127.0.0.1', port=8081)
