#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
from http.server import BaseHTTPRequestHandler, HTTPServer

import pika

def run():
    print('starting server...')

    server_address = ('127.0.0.1', 8082)
    httpd = HTTPServer(server_address, BaseHTTPRequestHandler)
    print('running server...')

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=sys.argv[1]))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, props, body):
        print(" [x] Received %r" % body)
        with open("db.txt", 'a') as f:
            f.write(str(body) + "\n")

    channel.basic_consume(callback,
                  queue='hello')
    
    channel.start_consuming()
    httpd.serve_forever()
    
run()

