#!/usr/bin/env python
# -*- coding: utf-8 -*-

import aiohttp
import asyncio
import async_timeout
import sys

@asyncio.coroutine
def fetch(session, url, data):
    with async_timeout.timeout(30):
        response = yield from session.post(url,data=data)
        return (yield from response.text())

@asyncio.coroutine
def main():
    url = "http://127.0.0.1:8081/"
    key = sys.argv[1]
    url += key
    value = int(sys.argv[2])
    data = {key:value}
    session = aiohttp.ClientSession()
    html = yield from fetch(session, url, data)
    print(html)
    session.close()
    
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
